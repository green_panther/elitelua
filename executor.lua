-- -------------------------------------------------------------------------- --
-- The MIT License (MIT)
--
-- Copyright (c) 2015 by Randshot
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included in all
-- copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
-- SOFTWARE.
-- -------------------------------------------------------------------------- --

-- -------------------------------------------------------------------------- --
-- Important globals
-- -------------------------------------------------------------------------- --
executorVersion = "v1.1.00"
scriptVersion = ""
gameVersion = ""

if not script then script = {} end
script.settings = getSettings('EliteLua')
script.ed = "EliteDangerous32.exe"
script.wd = "WatchDog.exe"
script.base = ""
script.extbase = 0

userdir = os.getenv("USERPROFILE")
eliteoutdir = userdir .. [[\Documents\EliteLua\]]
datadir = eliteoutdir .. [[Data\]]
logfiledir = eliteoutdir .. [[Logs\]]

logfilename = logfiledir .. 'debugLog.' .. os.date('%y%m%d%H%M') .. '.log'
udpfilename = logfiledir .. 'udpLog.' .. os.date('%y%m%d%H%M') .. '.log'

-- -------------------------------------------------------------------------- --
-- Setting up UI
-- -------------------------------------------------------------------------- --
function toggleCE(sender)
  if not state and not script.debugging then
    hideAllCEWindows()
    state = true
  elseif state then
    ui = nil
    state = false
    print = _print
    object_destroy(sender)
    unhideMainCEwindow()
  else
    state = true
  end
end

toggleCE()
-- Creating basic ui elements
ui = {}
ui.form = createForm(true)
ui.panel = createPanel(ui.form)
ui.output = createMemo(ui.form)
ui.input = createEdit(ui.form)

-- Defining the properties
-- -- form
form_centerScreen(ui.form)
form_onClose(ui.form, toggleCE)

setProperty(ui.form, "BiDiMode", "bdLeftToRight")
setProperty(ui.form, "Caption", "EliteLua")

control_setSize(ui.form, 450, 350)

-- -- panel
x,y = control_getSize(ui.form)
control_setSize(ui.panel, x, y)

-- -- output
ui.output.setWordWrap(false)
ui.output.setScrollbars(6)
setProperty(ui.output, "BorderStyle", "bsSingle")

control_setSize(ui.output, x * 0.94, (y * 0.94) / 1.1)
control_setPosition(ui.output, x * 0.03 , y * 0.03)

-- -- input
ui.input.OnKeyPress = function (sender, key)
  local vk = string.byte(key)
  if (vk == VK_RETURN) then
    if pInput(control_getCaption(sender)) then
      sender.clear()
    end
  elseif (vk == 46) then -- period
    if inputHistory then
      if not inputIndex then inputIndex = #inputHistory else if inputIndex > 1 then inputIndex = inputIndex - 1 end end
      control_setCaption(sender, inputHistory[inputIndex])
      doKeyPress(35)
      return
    end
  elseif (vk == 45) then -- minus
    if inputHistory then
      if not inputIndex then inputIndex = #inputHistory else if inputIndex < #inputHistory then inputIndex = inputIndex + 1 end end
      control_setCaption(sender, inputHistory[inputIndex])
      doKeyPress(35)
      return
    end
  end
  return key
end
setProperty(ui.input, "BorderStyle", "bsSingle")

control_setSize(ui.input, x * 0.94, 0)
x,y = control_getSize(ui.output)
control_setPosition(ui.input, x * 0.033  , y * 1.06)
x,y = nil

-- -------------------------------------------------------------------------- --
-- Custom print method
-- -------------------------------------------------------------------------- --
if not _print then _print = print end
if not format then format = string.format end
if not fprint then fprint = function (str, ...) print(format(str, ...)) end end
function print(...)
  local function split(s, delimiter)
    result = {};
    for match in (s..delimiter):gmatch("(.-)["..delimiter.."]") do
        table.insert(result, match);
    end
    return result;
  end
  local strTable = split(..., '\n')
  for i = 1,#strTable do
    local str = strTable[i]
    memo_append(ui.output, str)
    writeFile(logfilename, "a+", format("[%s] %s", tostring(os.date()), str .. "\n"))
    if script.debugging then _print(str) end
  end
end

-- -------------------------------------------------------------------------- --
-- Log method
-- -------------------------------------------------------------------------- --
function log(...)
  local str = ... .. "\n"
  writeFile(logfilename, "a+", format("[%s] %s", tostring(os.date()), str))
end

-- -------------------------------------------------------------------------- --
-- Command input handler
-- -------------------------------------------------------------------------- --
function pInput(_)
  if not script.detached then print("error, commands are not available until the script is done with initializing!") return false end
  if not inputHistory then inputHistory = {} inputHistory[1] = _ else inputHistory[#inputHistory + 1] = _ end
  inputIndex = #inputHistory + 1
  local input = {}
  local index = 1
  for str in string.gmatch(_, "([^%s]+)") do
    input[index] = str
    index = index + 1
  end

  if cmdTable['alias'].state then
    return aliasFunc(_)
  end

  local function rec(__, count, tbl)
    if not cmdTable[__[1]] then print("ERROR: Please input a valid command! Type 'list' for a list of commands.") return false end
    if #__ == 1 and not cmdTable[__[1]].iscmd then print("ERROR: Please input a valid command! Type 'list' for a list of commands.") return false end
    local ltbl = (type(tbl[__[#__ - (count - 1)]]) == 'table' and tbl[__[#__ - (count - 1)]]) or
      (type(tonumber(__[#__ - (count - 1)])) == 'number' and tbl)
    if ltbl then
      if count == 1 then
        if type(tonumber(__[#__])) == 'number' then
          return ltbl.func(__[#__])
        else
          return ltbl.func()
        end
      else
        count = count - 1
        return rec(__, count, ltbl)
      end
    elseif __[1] then
      print("ERROR: Please input a valid command! Type 'list' for a list of commands.")
      return false
    else
      return true
    end
  end

  if not cmdTable then print("ERROR: No commands loaded!") return false end
  return rec(input, #input, cmdTable)
end

-- -------------------------------------------------------------------------- --
-- File handling related functions
-- -------------------------------------------------------------------------- --
function exists(name)
    if (type(name) ~= "string") then return false end
    return os.rename(name, name) and true or false
end

function isFile(name)
    if type(name) ~= "string" then return false end
    if (not exists(name)) then return false end
    local f = io.open(name)
    if (f) then
        f:close()
        return true
    end
    return false
end

function isDir(name) return (exists(name) and not isFile(name)) end
function readFile(f, mode) local r = io.open(f, mode or 'rb') local ret = r:read('*all') r:close() return ret end
function writeFile(f, mode, str) local r = io.open(f, mode or 'w+') r:write(str) r:close() end

if not isDir(eliteoutdir) then
  os.execute([[md "]] .. eliteoutdir .. [["]])
  os.execute([[md "]] .. datadir .. [["]])
  os.execute([[md "]] .. logfiledir .. [["]])

elseif (not isDir(datadir)) or (not isDir(logfiledir)) then
  os.execute([[md "]] .. datadir .. [["]])
  os.execute([[md "]] .. logfiledir .. [["]])
end

-- -------------------------------------------------------------------------- --
-- HTTP GET -> for downloading the data
-- -------------------------------------------------------------------------- --
function httpGet(url, file)
  assert(type(url) == 'string', 'has to be an url')
  local tmpfile = datadir .. "_tmp.data"
  file = type(file) == 'string' and datadir .. file or tmpfile
  local result = ""

  local cmd = format([[powershell -windowstyle hidden -command "& { (New-Object Net.WebClient).DownloadFile('%s', '%s') }"]], url, tmpfile)
  cmd = assert(io.popen(cmd, 'r'))
  local response = cmd:read('*a')
  cmd:close()

  if (response:len() == 0) then
    result = readFile(tmpfile)
    writeFile(file, "w+", result)
    if (isFile(tmpfile)) then
      os.remove(tmpfile)
    end

  else
    if (isFile(file)) then
      result = readFile(file)
      fprint("Couldn't update data! Using saved data!")

    else
      fprint("Couldn't update data! No saved data found!")
    end
  end
  return result
end

-- -------------------------------------------------------------------------- --
-- Loads the requested data into the script
-- -------------------------------------------------------------------------- --
function loadData(dataname)
  local file = datadir .. "data.lua"
  if (isFile(file)) then
    local dataTable = loadstring(readFile(file))()
    return dataTable[dataname]
  else
    local dataUrl = [[http://urealm.eu/data]]
    local dataTable = loadstring(httpGet(dataUrl, "data.lua"))()
    return dataTable[dataname]
  end
end

-- -------------------------------------------------------------------------- --
-- Downloads the data again
-- -------------------------------------------------------------------------- --
function updateData()
  local dataUrl = [[http://urealm.eu/data]]
  httpGet(dataUrl, "data.lua")
end

-- -------------------------------------------------------------------------- --
-- Deletes the current settings
-- -------------------------------------------------------------------------- --
function deleteSettings()
  for k,v in pairs(script.settings.Value) do
    script.settings.Value[k] = nil
  end
  print("Settings deleted!")
end

-- -------------------------------------------------------------------------- --
-- Displays the current versions
-- -------------------------------------------------------------------------- --
script.version = loadData("version")
script.version()
print("Executor Version: " .. executorVersion)
print("EliteLua Version: " .. scriptVersion)
print("For Game Version: " .. gameVersion)
print("Notice: It is advised to NOT use this in Open Play!")

-- -------------------------------------------------------------------------- --
-- Loading various functions
-- -------------------------------------------------------------------------- --
script.functions = loadData("functions")
script.functions()
if not cmdTable then cmdTable = orderedTable { __name = "cmdTable"} end

-- -------------------------------------------------------------------------- --
-- If needed, displays the change log
-- -------------------------------------------------------------------------- --
if (script.settings.Value['version'] ~= scriptVersion) then
  script.changelog = loadData("changelog")
  print("")
  for line in string.gmatch(script.changelog(scriptVersion), "%C+") do
    print(line)
  end
  script.settings.Value['version'] = scriptVersion
end

-- -------------------------------------------------------------------------- --
-- Function for manually printing the change log
-- -------------------------------------------------------------------------- --
function showChangelog(version)
  if not version then
    for line in string.gmatch(script.changelog(), "%C+") do
      print(line)
    end
  else
    for line in string.gmatch(script.changelog(version), "%C+") do
      print(line)
    end
  end
end

-- -------------------------------------------------------------------------- --
-- Loading initiate function
-- -------------------------------------------------------------------------- --
script.init = loadData("init")

-- -------------------------------------------------------------------------- --
-- Doing prep work
-- -------------------------------------------------------------------------- --
script.attach = loadData("attach")
script.attach()
