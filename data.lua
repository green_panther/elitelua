return {
version = function()
  scriptVersion = "v1.0.08"
  gameVersion = "v1.3.08"
end,

changelog = function(version)
  local changeTable = orderedTable { __name = "Changelog" }
  changeTable['v1.0.00'] = [[
    List of changes for v1.0.00:
      Inital Release!
  ]]
  changeTable['v1.0.01'] = [[
    List of changes for v1.0.01:
      hotfix: hull damage
  ]]
  changeTable['v1.0.02'] = [[
    List of changes for v1.0.02:
      added: alias command creation/deletion
      added: alias commands will be saved
      removed: junk code
  ]]
  changeTable['v1.0.03'] = [[
    List of changes for v1.0.03:
      added: alias list -> will list all saved alias commands
      added: alias reset -> will delete all saved alias commands
      changed: an alias can now hold multiple commands
  ]]
  changeTable['v1.0.04'] = [[
    List of changes for v1.0.04:
      fix: fixed alias saving and loading
      fix: fixed module damage negation
      changed: updated command list to include new alias commands
      added: status command: prints a status list of all commands
  ]]
  changeTable['v1.0.05'] = [[
    List of changes for v1.0.05:
      added: no fuel usage for jumps
      added: reduced fuel usage for jumps with factor
  ]]
  changeTable['v1.0.06'] = [[
    List of changes for v1.0.06:
      added: saving/loading/deleting/listing of settings
      added: you can set a default setting which will be loaded upon script start
      added: module system
  ]]
  changeTable['v1.0.07'] = [[
    List of changes for v1.0.07:
      fix: loading of modules since Lua is being bada**
      changed: renamed the terminate command into exit
      changed: slight syntax change for the modules
  ]]
  changeTable['v1.0.08'] = [[
    List of changes for v1.0.08:
      added: help command (replacement for list/status)
      added: autoupdate toggle -> Will check for updates on every start
      added: modules require an description tag now
      removed: list command
      removed: status command
      removed: combatmode (use alias for own one)
      removed: jumpmode (use alias for own one)
  ]]

  if (type(version) == 'string') then
    result = changeTable[version]
  else
    local changelist = ""
    for k,v in ordered(changeTable) do
      changelist = format("%s\n%s", changelist, changeTable[k])
    end
    result = changelist
  end
  return result
end,

functions = function()
  function readAddress(a)
    local v = readInteger(a)
    if (targetIs64Bit()) then
      return v
    elseif (v ~= nil) then
      return v % 0x100000000
    else
      return nil
    end
  end

  function hex2float(n)
    return byteTableToFloat(dwordToByteTable(n))
  end

  function float2hex(n)
    return byteTableToDword(floatToByteTable(n))
  end

  function hex2double(n)
    return byteTableToDouble(qwordToByteTable(n))
  end

  function double2hex(n)
    return byteTableToQword(doubleToByteTable(n))
  end

  function dec2hex(n)
    return format("%x", n) .. ""
  end

  function hex2dec(n)
    return tonumber(n, 16) .. ""
  end

  function string:split(pattern, out)
    if not out then out = {} end
    if self == "" then return out end
    local start = 1
    local splitStart, splitEnd = string.find(self, pattern, start)
    if not splitStart then table.insert(out, self) return out end
    while splitStart do
      if splitStart == self:len() then break end
      table.insert(out, string.sub(self, start, splitStart - 1))
      start = splitEnd + 1
      splitStart, splitEnd = string.find(self, pattern, start)
    end
    table.insert(out, string.sub(self, start))
    return out
  end

  function os.capture(cmd, raw)
    local f = assert(io.popen(cmd, 'r'))
    local s = assert(f:read('*a'))
    f:close()
    if raw then return s end
    s = string.gsub(s, '^%s+', '')
    s = string.gsub(s, '%s+$', '')
    s = string.gsub(s, '[\n\r]+', ' ')
    return s
  end

  function getRandomFloat(min, max)
    return min + math.random() * (max - min)
  end

  function orderedTable(t)
    local currentIndex = 1
    local metaTable = {}

    function metaTable:__newindex(key, value)
      rawset(self, key, value)
      rawset(self, currentIndex, key)
      currentIndex = currentIndex + 1
    end
    return setmetatable(t or {}, metaTable)
  end

  function ordered(t)
    local currentIndex = 0
    local function iter(t)
      currentIndex = currentIndex + 1
      local key = t[currentIndex]
      if key then return key, t[key] end
    end
    return iter, t
  end

  function isNPC(arg)
    if (arg == 0) then
      return true
    end

    local shipObj = readAddress(arg + 0x160)
    if (shipObj == nil or shipObj == 0) then
      return true
    end

    local flag = readBytes(shipObj + 152, 1, false)
    if (flag == 1) then
      return true
    elseif (flag == 0) then
      return false
    end

    log("NPC flag error: " .. tostring(flag))
    return false
  end

  nonRepHotkeysList = {}
  function createNonRepHotkey(func, ...)

    local changedBehaviour =
      function(sender)
        local ID = userDataToInteger(sender)
        local TC = getTickCount()
        local elapTicks = TC - nonRepHotkeysList[ID]

        if (elapTicks > 300) then func(sender) end

        nonRepHotkeysList[ID] = TC
      end

    local hk = createHotkey(changedBehaviour, ...)
    hk.DelayBetweenActivate = 10
    nonRepHotkeysList[userDataToInteger(hk)] = getTickCount()

    return hk
  end

  -- -------------------------------------------------------------------------- --
  -- Adds breakpoint toggles to the command list
  -- -------------------------------------------------------------------------- --
  function addBreakpoint(name, address, breakFunc, desc, parent, onFunc, offFunc, customFunc)
    if cmdTable and not cmdTable[name] then
      cmdTable[name] = {
        ['on'] = {func = function ()
          if cmdTable[name].parent then
            if not cmdTable[parent].state then
              fprint("%s: error, please turn on the parent (%s) first!", name, parent)
              return true
            end
          end
          if not cmdTable[name].state then
            if onFunc then
              if not onFunc() then
                return false
              end
            end
            debug_setBreakpoint(address)
            cmdTable[name].state = true
            breakpointTable[name] = true
            fprint("--> %s: on", name)
            return true
          else
            fprint("--> %s: already on", name)
            return true
          end
        end};
        ['off'] = {func = function ()
          if cmdTable[name].child then
            for i = 1,#cmdTable[name].child do
              if cmdTable[cmdTable[name].child[i]].state then
                fprint("%s: error, please turn off the child (%s) first!", name, cmdTable[name].child[i])
                return true
              end
            end
          end
          if cmdTable[name].state then
            if offFunc then
              if not offFunc() then
                return false
              end
            end
            debug_removeBreakpoint(address)
            cmdTable[name].state = false
            breakpointTable[name] = false
            fprint("--> %s: off", name)
            return true
          else
            fprint("--> %s: already off", name)
            return true
          end
        end};
      }
      if desc then cmdTable[name].desc = desc end
      if parent then
        cmdTable[name].parent = parent
        if cmdTable[parent] then
          if not cmdTable[parent].child then cmdTable[parent].child = {} end
          table.insert(cmdTable[parent].child, name)
        else
          fprint("--> %s: error, please add the parent (%s) before the child!", name, parent)
          return false
        end
      end
      if customFunc then customFunc() end
      if not breakpointTable then breakpointTable = orderedTable { __name = "breakpointTable" } end
      cmdTable[name].address = address
      debug_removeBreakpoint(address)
      cmdTable[name].breakpointFunc = breakFunc
    elseif not cmdTable then
      fprint("--> %s: error, no cmd table found!", name)
      return false
    else
      fprint("--> %s: error, already added!", name)
      return true
    end
    log(format("--> %s: successfully created!", name))
    return true
  end

  -- -------------------------------------------------------------------------- --
  -- Adds hook toggles to the command list
  -- -------------------------------------------------------------------------- --
  function addHook(name, address, aaCustom, funcCustom, desc, parent, onFunc, offFunc)
    if cmdTable and not cmdTable[name] then
      cmdTable[name] = {
        ['on'] = {func = function ()
          if cmdTable[name].parent then
            if not cmdTable[parent].state then
              fprint("%s: error, please turn on the parent (%s) first!", name, parent)
              return true
            end
          end
          if not cmdTable[name].state then
            if onFunc then
              if not onFunc() then
                return false
              end
            end
            autoAssemble(cmdTable[name].aaenable)
            cmdTable[name].state = true
            fprint("--> %s: on", name)
            return true
          else
            fprint("--> %s: already on", name)
            return true
          end
        end};
        ['off'] = {func = function ()
          if cmdTable[name].child then
            for i = 1,#cmdTable[name].child do
              if cmdTable[cmdTable[name].child[i]].state then
                fprint("%s: error, please turn off the child (%s) first!", name, cmdTable[name].child[i])
                return true
              end
            end
          end
          if cmdTable[name].state then
            if offFunc then
              if not offFunc() then
                return false
              end
            end
            autoAssemble(cmdTable[name].aadisable)
            cmdTable[name].state = false
            fprint("--> %s: off", name)
            return true
          else
            fprint("--> %s: already off", name)
            return true
          end
        end};
      }
      if desc then cmdTable[name].desc = desc end
      if parent then
        cmdTable[name].parent = parent
        if cmdTable[parent] then
          if not cmdTable[parent].child then cmdTable[parent].child = {} end
          table.insert(cmdTable[parent].child, name)
        else
          fprint("--> %s: error, please add the parent (%s) before the child!", name, parent)
          return false
        end
      end
      cmdTable[name].address = address
      if funcCustom then funcCustom() end
      createHook(name, address)
      autoAssemble(aaCustom)
    elseif not cmdTable then
      fprint("--> %s: error, no cmd table found!", name)
      return false
    else
      fprint("--> %s: error, already added!", name)
      return true
    end
    log(format("--> %s: successfully created!", name))
    return true
  end

  -- -------------------------------------------------------------------------- --
  -- Adds patch toggles to the command list
  -- -------------------------------------------------------------------------- --
  function addPatch(name, address, bytes, desc, parent, onFunc, offFunc)
    if cmdTable and not cmdTable[name] then
      cmdTable[name] = {
        ['on'] = {func = function ()
          if cmdTable[name].parent then
            if not cmdTable[parent].state then
              fprint("%s: error, please turn on the parent (%s) first!", name, parent)
              return true
            end
          end
          if not cmdTable[name].state then
            if onFunc then
              if not onFunc() then
                return false
              end
            end
            autoAssemble(cmdTable[name].aaenable)
            cmdTable[name].state = true
            fprint("--> %s: on", name)
            return true
          else
            fprint("--> %s: already on", name)
            return true
          end
        end};
        ['off'] = {func = function ()
          if cmdTable[name].child then
            for i = 1,#cmdTable[name].child do
              if cmdTable[cmdTable[name].child[i]].state then
                fprint("%s: error, please turn off the child (%s) first!", name, cmdTable[name].child[i])
                return true
              end
            end
          end
          if cmdTable[name].state then
            if offFunc then
              if not offFunc() then
                return false
              end
            end
            autoAssemble(cmdTable[name].aadisable)
            cmdTable[name].state = false
            fprint("--> %s: off", name)
            return true
          else
            fprint("--> %s: already off", name)
            return true
          end
        end};
      }
      if desc then cmdTable[name].desc = desc end
      if parent then
        cmdTable[name].parent = parent
        if cmdTable[parent] then
          if not cmdTable[parent].child then cmdTable[parent].child = {} end
          table.insert(cmdTable[parent].child, name)
        else
          fprint("--> %s: error, please add the parent (%s) before the child!", name, parent)
          return false
        end
      end
      cmdTable[name].address = address
      createPatch(name, address, bytes)
    elseif not cmdTable then
      fprint("--> %s: error, no cmd table found!", name)
      return false
    else
      fprint("--> %s: error, already added!", name)
      return true
    end
    log(format("--> %s: successfully created!", name))
    return true
  end

  -- -------------------------------------------------------------------------- --
  -- Adds normal toggles to the command list
  -- -------------------------------------------------------------------------- --
  function addToggle(name, desc, parent, onFunc, offFunc)
    if cmdTable and not cmdTable[name] then
      cmdTable[name] = {
        ['on'] = {func = function ()
          if cmdTable[name].parent then
            if not cmdTable[parent].state then
              fprint("%s: error, please turn on the parent (%s) first!", name, parent)
              return true
            end
          end
          if not cmdTable[name].state then
            if onFunc then
              if not onFunc() then
                return false
              end
            end
            cmdTable[name].state = true
            fprint("--> %s: on", name)
            return true
          else
            fprint("--> %s: already on", name)
            return true
          end
        end};
        ['off'] = {func = function ()
          if cmdTable[name].child then
            for i = 1,#cmdTable[name].child do
              if cmdTable[cmdTable[name].child[i]].state then
                fprint("%s: error, please turn off the child (%s) first!", name, cmdTable[name].child[i])
                return true
              end
            end
          end
          if cmdTable[name].state then
            if offFunc then
              if not offFunc() then
                return false
              end
            end
            cmdTable[name].state = false
            fprint("--> %s: off", name)
            return true
          else
            fprint("--> %s: already off", name)
            return true
          end
        end};
      }
      if desc then cmdTable[name].desc = desc end
      if parent then
        cmdTable[name].parent = parent
        if cmdTable[parent] then
          if not cmdTable[parent].child then cmdTable[parent].child = {} end
          table.insert(cmdTable[parent].child, name)
        else
          fprint("--> %s: error, please add the parent (%s) before the child!", name, parent)
          return false
        end
      end
    elseif not cmdTable then
      fprint("--> %s: error, no cmd table found!", name)
      return false
    else
      fprint("--> %s: error, already added!", name)
      return true
    end
    log(format("--> %s: successfully created!", name))
    return true
  end

  -- -------------------------------------------------------------------------- --
  -- Adds properties to already existing toggles to the command list
  -- -------------------------------------------------------------------------- --
  function addProperty(name, initVal, parent, setFunc, desc)
    if cmdTable and cmdTable[parent] then
      if not cmdTable[parent].child then cmdTable[parent].child = {} end
      if not cmdTable[parent].child[name] then cmdTable[parent].child[name] = {} end
      if desc then cmdTable[parent].child[name].desc = desc end
      if not cmdTable[parent].set then cmdTable[parent].set = {} end
      if not cmdTable[parent].reset then cmdTable[parent].reset = {} end
      cmdTable[parent].set[name] = {func = function (val)
        print(setFunc(val))
        return true
      end}
      cmdTable[parent].reset[name] = {func = function ()
        print(setFunc(initVal))
        return true
      end}
    elseif not cmdTable then
      fprint("--> %s: error, no cmd table found!", name)
      return false
    else
      fprint("--> %s: error, please add the parent (%s) first!", name, parent)
      return false
    end
    setFunc(initVal)
    log(format("--> %s: successfully created!", name))
    return true
  end

  function dec(_)
    local extbase = script.extbase
    local base = script.base
    return _ - extbase + base
  end

  function addCMD(name, cmdFunc, desc, funcArg)
    if cmdTable and not cmdTable[name] then
      if funcArg then
        cmdTable[name] = {func = cmdFunc(funcArg)}
      else
        cmdTable[name] = {func = cmdFunc}
      end
      if desc then cmdTable[name].desc = desc end
    elseif not cmdTable then
      fprint("--> %s: error, no cmd table found!", name)
      return false
    else
      fprint("--> %s: error, already added!", name)
      return true
    end
    log(format("--> %s: successfully created!", name))
    cmdTable[name].iscmd = true
    return true
  end

  function aliasFunc(_)
    if not cmdTable['alias'] then
      cmdTable['alias'] = {
        create = {func = function()
          cmdTable['alias'].state = "create"
          cmdTable['alias'].stage = 1
          print("alias: what should your alias be called (abort with blank input):")
          return true
        end};

        delete = {func = function()
          cmdTable['alias'].state = "delete"
          print("alias: what's the name of the alias you want to delete (abort with blank input):")
          return true
        end};

        list = {func = function()
          local aliasTable = script.settings.Value['aliastable']:split(",")
          if not aliasTable[1] then print("alias: there are no saved alias commands!") return true end
          for i = 1, #aliasTable do
            local nameTable = aliasTable[i]:split(":")
            local commandTable = nameTable[2]:split("-")
            fprint("\n%s:\n - %s", nameTable[1], table.concat(commandTable, "\n - "))
          end
          return true
        end};

        reset = {func = function()
          script.settings.Value['aliastable'] = nil
          print("alias: deleted all alias commands!")
          return true
        end};
        desc = {
          create = "Creates an alias under a name",
          reset = "Deletes all created aliases",
          delete = "Deletes an alias by name",
          list = "Lists all available aliases"
        }
      }
      cmdTable['alias'].iscustom = true
      local aliasTable = script.settings.Value['aliastable']:split(",")
      if not aliasTable[1] then return true end
      for i = 1, #aliasTable do
        local nameTable = aliasTable[i]:split(":")
        local commandTable = nameTable[2]:split("-")
        if not cmdTable[nameTable[1]] then
          local afuncstr = ""
          for i2 = 1, #commandTable do afuncstr = format("%s\n  pInput('%s')", afuncstr, commandTable[i2]) end
          afuncstr = format("%s%s\n%s", "return function()", afuncstr, "return true end")
          local afunc = loadstring(afuncstr)()
          cmdTable[nameTable[1]] = {func = afunc}
          cmdTable[nameTable[1]].iscmd = true
          fprint("alias: alias (%s) loaded!", nameTable[1])
        else
          log(format("alias: error, this name (%s) is already occupied!", nameTable[1]))
        end
      end
      return true
    elseif cmdTable['alias'].state == "settings" then
      settingsFunc(_)
    elseif _ == "" then
      if script.createlist then
        local afuncstr = ""
        for i = 1,#script.createlist do afuncstr = format("%s\n  pInput('%s')", afuncstr, script.createlist[i]) end
        afuncstr = format("%s%s\n%s", "return function()", afuncstr, "return true end")
        local afunc = loadstring(afuncstr)()
        cmdTable[cmdTable['alias'].temp] = {func = afunc}
        cmdTable[cmdTable['alias'].temp].iscmd = true

        local aliasTable = script.settings.Value['aliastable']:split(",")
        local nameTable = {}
        for i = 1, #aliasTable do
          nameTable[i] = aliasTable[i]:split(":")
        end
        nameTable[#nameTable + 1] = {cmdTable['alias'].temp, table.concat(script.createlist, "-")}
        for i = 1, #aliasTable + 1 do
          aliasTable[i] = table.concat(nameTable[i], ":")
        end
        script.settings.Value['aliastable'] = table.concat(aliasTable, ",")
        cmdTable['alias'].state = nil
        script.createlist = nil
        fprint("alias: alias (%s) successfully created and saved!", cmdTable['alias'].temp)
        return true
      else
        print("Aborted!")
        cmdTable['alias'].state = nil
      end
    elseif cmdTable['alias'].state == "create" then
      if cmdTable['alias'].stage == 1 then
        if not cmdTable[_] then
          cmdTable['alias'].temp = _
          cmdTable['alias'].stage = 2
          print("alias: type in your first command:")
          return true
        else
          fprint("alias: error, this name (%s) is already occupied! Please choose a different one!", _)
          return false
        end
      else
        if not script.createlist then script.createlist = {} end
        script.createlist[#script.createlist + 1] = _
        fprint("alias: '%s' added!", _)
        print("alias: Anything else? (finish with blank line):")
        return true
      end
    else
      if script.settings.Value['aliastable']:find(_) then
        cmdTable[_] = nil
        local aliasTable = script.settings.Value['aliastable']:split(",")
        local nameTable = {}
        for i = 1, #aliasTable do
          nameTable[i] = aliasTable[i]:split(":")
          if nameTable[i][1] == _ then
            table.remove(aliasTable, i)
          end
        end
        script.settings.Value['aliastable'] = table.concat(aliasTable, ",")
        cmdTable['alias'].state = nil
        fprint("alias: alias (%s) successfully deleted!", _)
        return true
      else
        fprint("alias: error, no such alias (%s) was created!", _)
        return false
      end
    end
    return true
  end

  function settingsFunc(_)
    -- name>settingName:value-settingName2:value2,name2>settingName:value...
    if not cmdTable['settings'] then
      cmdTable['settings'] = {
        save = {func = function()
          cmdTable['alias'].state = "settings"
          cmdTable['settings'].state = "save"
          print("settings: what should your setting be called (abort with blank input):")
          return true
        end};

        load = {func = function()
          cmdTable['alias'].state = "settings"
          cmdTable['settings'].state = "load"
          print("settings: what is the name of the setting you want to load (abort with blank input):")
          return true
        end};

        delete = {func = function()
          cmdTable['alias'].state = "settings"
          cmdTable['settings'].state = "delete"
          print("settings: what is the name of the setting you want to delete (abort with blank input):")
          return true
        end};

        setdefault = {func = function()
          cmdTable['alias'].state = "settings"
          cmdTable['settings'].state = "setdefault"
          print("settings: what is the name of the setting you want to set for your default (abort with blank input):")
          return true
        end};

        list = {func = function()
          local settingsTable = script.settings.Value['settingstable']:split(",")
          if not settingsTable[1] then print("settings: there are no saved settings!") return true end
          for i = 1, #settingsTable do
            local nameTable = settingsTable[i]:split(">")
            local snameTable = nameTable[2]:split("-")
            fprint("\n%s:", nameTable[1])
            for i = 1, #snameTable do
              local valueTable = snameTable[i]:split(":")
              fprint(" > %s: %s", valueTable[1], valueTable[2])
            end
          end
          return true
        end};
        desc = {
          save = "Saves your current configuration under a name",
          load = "Loads a configuration by name",
          delete = "Deletes a configuration by name",
          setdefault = "Sets a default configuration by name",
          list = "Lists all available configurations"
        }
      }
      cmdTable['settings'].iscustom = true
      print("Loading your default settings...")
      local name = script.settings.Value['defaultsetting']
      if name == "" then print("No default settings defined...\nDone!") return true end
      if script.settings.Value['settingstable']:find(name) then
        local settingsTable = script.settings.Value['settingstable']:split(",")
        for i = 1, #settingsTable do
          local nameTable = settingsTable[i]:split(">")
          if nameTable[1] == name then
            cmdTable['settings'].state = nil
            cmdTable['alias'].state = nil
            local snameTable = nameTable[2]:split("-")
            for i = 1, #snameTable do
              local valueTable = snameTable[i]:split(":")
              if valueTable[2] == "true" then
                pInput(format("%s on", valueTable[1]))
              elseif valueTable[2] ~= "false" then
                local propTable = valueTable[2]:split("#")
                pInput(format("%s set %s %s", propTable[1], valueTable[1], propTable[2]))
              end
            end
          end
        end
        fprint("Default settings (%s) loaded!\nDone!", name)
        return true
      else
        fprint("settings: error, there is no setting with the name: %s", name)
        return false
      end
      return true
    elseif _ ~= "" then
      if cmdTable['settings'].state == "save" then
        local name = _
        local settingsTable = script.settings.Value['settingstable']:split(",")
        if script.settings.Value['settingstable']:find(_) then
          for i = 1, #settingsTable do
            local nameTable = settingsTable[i]:split(">")
            if nameTable[1] == _ then
              fprint("settings: error, this name (%s) is already occupied! Please choose a different one!", _)
              return false
            end
          end
        end
        local valueTable = {}
        local snameTable = {}
        local nameTable = {}
        for k,v in ordered(cmdTable) do
          if cmdTable[k].state and not cmdTable[k].iscmd then
            valueTable[1] = k
            valueTable[2] = tostring(cmdTable[k].state)
            snameTable[#snameTable + 1] = table.concat(valueTable, ":")
          end
          if cmdTable[k].set then
            for prop in pairs(cmdTable[k].set) do
              valueTable[1] = prop
              valueTable[2] = format("%s#%s", k, tostring(cmdTable[k].set[prop].value))
              snameTable[#snameTable + 1] = table.concat(valueTable, ":")
            end
          end
        end
        nameTable[1] = name
        nameTable[2] = table.concat(snameTable, "-")
        settingsTable[#settingsTable + 1] = table.concat(nameTable, ">")
        script.settings.Value['settingstable'] = table.concat(settingsTable, ",")
        fprint("settings: settings saved under the name: %s", _)
        cmdTable['settings'].state = nil
        cmdTable['alias'].state = nil
        return true
      elseif cmdTable['settings'].state == "load" then
        if script.settings.Value['settingstable']:find(_) then
          local settingsTable = script.settings.Value['settingstable']:split(",")
          for i = 1, #settingsTable do
            local nameTable = settingsTable[i]:split(">")
            if nameTable[1] == _ then
              cmdTable['settings'].state = nil
              cmdTable['alias'].state = nil
              local snameTable = nameTable[2]:split("-")
              for i = 1, #snameTable do
                local valueTable = snameTable[i]:split(":")
                if valueTable[2] == "true" then
                  pInput(format("%s on", valueTable[1]))
                elseif valueTable[2] ~= "false" then
                  local propTable = valueTable[2]:split("#")
                  pInput(format("%s set %s %s", propTable[1], valueTable[1], propTable[2]))
                end
              end
            end
          end
          fprint("settings: settings (%s) loaded!", _)
          cmdTable['settings'].state = nil
          cmdTable['alias'].state = nil
          return true
        else
          fprint("settings: there is no setting with the name: %s", _)
          return false
        end
      elseif cmdTable['settings'].state == "delete" then
        if script.settings.Value['settingstable']:find(_) then
          local settingsTable = script.settings.Value['settingstable']:split(",")
          for i = 1, #settingsTable do
            local nameTable = settingsTable[i]:split(">")
            if nameTable[1] == _ then
              script.settings.Value['defaultsetting'] = ""
              table.remove(settingsTable, i)
              break
            end
          end
          script.settings.Value['settingstable'] = table.concat(settingsTable, ",")
          cmdTable['settings'].state = nil
          cmdTable['alias'].state = nil
          fprint("settings: setting (%s) deleted!", _)
          return true
        else
          fprint("settings: there is no setting with the name: %s", _)
          return false
        end
      elseif cmdTable['settings'].state == "setdefault" then
        if script.settings.Value['settingstable']:find(_) then
          script.settings.Value['defaultsetting'] = _
          fprint("settings: setting (%s) set for default!", _)
          cmdTable['settings'].state = nil
          cmdTable['alias'].state = nil
          return true
        else
          fprint("settings: there is no setting with the name: %s", _)
          return false
        end
      end
    else
      print("Aborted!")
      cmdTable['settings'].state = nil
      cmdTable['alias'].state = nil
    end
  end

  function moduleFunc()
    local moduledir = eliteoutdir .. [[Modules\]]
    if not isDir(moduledir) then
      os.execute([[md "]] .. moduledir .. [["]])
      log("Created module dir")
    end
    local outTable = os.capture([[dir /b ]] .. moduledir):split(" ")
    local modTable = {}
    for i = 1, #outTable do
      modTable[i] = readFile(moduledir .. outTable[i])
      if modTable[i] then
        local userTable = loadstring("return {\n" .. modTable[i] .. "\n}")()
        local ctype = userTable['ctype']
        if ctype == "breakpoint" then
          addBreakpoint(userTable['name'], userTable['address'], userTable['breakFunc'], userTable['description'], userTable['parent'], userTable['onFunc'], userTable['offFunc'], userTable['customFunc'])
          fprint("Module: %s loaded!", userTable['name'])
        elseif ctype == "hook" then
          addHook(userTable['name'], userTable['address'], userTable['aaFunc'], userTable['customFunc'], userTable['description'], userTable['parent'], userTable['onFunc'], userTable['offFunc'])
          fprint("Module: %s loaded!", userTable['name'])
        elseif ctype == "patch" then
          addPatch(userTable['name'], userTable['address'], userTable['bytes'], userTable['description'], userTable['parent'], userTable['onFunc'], userTable['offFunc'])
          fprint("Module: %s loaded!", userTable['name'])
        elseif ctype == "toggle" then
          addToggle(userTable['name'], userTable['description'], userTable['parent'], userTable['onFunc'], userTable['offFunc'])
          fprint("Module: %s loaded!", userTable['name'])
        elseif ctype == "property" then
          addProperty(userTable['name'], userTable['initVal'], userTable['parent'], userTable['setFunc'], userTable['description'])
          fprint("Module: %s loaded!", userTable['name'])
        elseif ctype == "cmd" then
          addCMD(userTable['name'], userTable['cmdFunc'], userTable['description'], userTable['arg'])
          fprint("Module: %s loaded!", userTable['name'])
        else
          print("Module: error, something went wrong! Check your syntax!")
        end
      end
    end
    print("")
  end

  -- -------------------------------------------------------------------------- --
  -- Creates patches
  -- -------------------------------------------------------------------------- --
  function createPatch(name, address, bytes)
    local count = 0
    for str in string.gmatch(bytes, "([^%s]+)") do
      count = count + 1
    end
    cmdTable[name].aadisable = dec2hex(address) .. ":\n db " .. unpack(readBytes(address, count, true))
    cmdTable[name].aaenable = format("%x:\ndb %s", address, bytes)
  end

  -- -------------------------------------------------------------------------- --
  -- Creates hooks
  -- -------------------------------------------------------------------------- --
  function createHook(name, address)
    local size = 0
    repeat
      size = size + getInstructionSize(address + size)
    until size >= 5
    local returnaddress = address + size

    local _bytes = readBytes(address, size, true)
    local bytes = ""
    for _, data in pairs(_bytes) do
      local hex = format("%x", data)
      if (hex:len() == 1) then
        hex = format("0%s", hex)
      end
      bytes = format("%s %s", bytes, hex)
    end

    local nops = ""
    if (size > 5) then
      for i = 1, size - 5 do
        nops = format("%s %s", nops, "90")
      end
      nops = format("%s%s", "db", nops)
    end

    local aahook = format([===[
    alloc(_%s,%i)
    alloc(%s,1024)
    alloc(__%s,4)
    registersymbol(_%s)
    registersymbol(%s)
    registersymbol(__%s)
    _%s:
    db%s
    JMP [__%s]
    %x:
    JMP %s
    %s
    __%s:
    dd %x]===],
    name, size + 6, name, name, name, name, name, name, bytes, name, address, name, nops, name, returnaddress)

    local aaenable = format([===[
    %x:
    JMP %s
    %s
    ]===],
    address, name, nops)

    local aadisable = format([===[
    %x:
    db%s
    ]===],
    address, bytes)

    cmdTable[name].aahook = aahook
    cmdTable[name].aaenable = aaenable
    cmdTable[name].aadisable = aadisable
    autoAssemble(cmdTable[name].aahook)
    autoAssemble(cmdTable[name].aadisable)
  end

  -- -------------------------------------------------------------------------- --
  -- Dissect the PE Header
  -- -------------------------------------------------------------------------- --
  function dissectPEHeader(module)
    local base = getAddress(module)
    local msdosSize = byteTableToDword(readBytes(base + 0x3C, 2, true))
    local headerBase = base + msdosSize
    local numOfSections = byteTableToDword(readBytes(headerBase + 6, 2, true))
    local optionalHeaderSize = byteTableToDword(readBytes(headerBase + 20, 2, true))
    local sectionArrayBase = headerBase + 24 + optionalHeaderSize
    local pe_header = {
      base = base;
      msdosSize = msdosSize;
      headerBase = headerBase;
      numOfSections = numOfSections;
      optionalHeaderSize = optionalHeaderSize;
      sectionArrayBase = sectionArrayBase;
    };
    for i = 0, numOfSections - 1 do
      local sectionBase = sectionArrayBase + i * 40
      local sectionName = readString(sectionBase, 8)
      pe_header[sectionName] = {
        name = sectionName;
        base = sectionBase;
        size = byteTableToDword(readBytes(sectionBase + 8, 4, true));
        address = byteTableToDword(readBytes(sectionBase + 12, 4, true));
        sizeOfRawData = byteTableToDword(readBytes(sectionBase + 16, 4, true));
        pointerToRawData = byteTableToDword(readBytes(sectionBase + 20, 4, true));
        pointerToRawRelocations = byteTableToDword(readBytes(sectionBase + 24, 4, true));
        pointerToLineNumbers = byteTableToDword(readBytes(sectionBase + 28, 4, true));
        numOfRelocations = byteTableToDword(readBytes(sectionBase + 32, 2, true));
        numOfLineNumbers = byteTableToDword(readBytes(sectionBase + 34, 2, true));
        characteristics = byteTableToDword(readBytes(sectionBase + 36, 4, true));
      };
    end

    return pe_header
  end
end,

hacks = function()
  local function helpFunc()
    print("") -- new line
    for k,v in ordered(cmdTable) do
      local result = ""
      if cmdTable[k].state then
        -- [ON]
        result = format("[ON] > %s <off>", k)
      elseif not cmdTable[k].iscmd and not cmdTable[k].iscustom then
        -- [OFF]
        result = format("[OFF] > %s <on>", k)
      elseif cmdTable[k].iscustom then
        -- [CUSTOM]
        result = format("[CUSTOM] > %s", k)
        print(result)
        for subc in pairs(cmdTable[k]) do
          if (subc ~= "iscustom") and (subc ~= "desc") then
            if cmdTable[k].desc and cmdTable[k].desc[subc] then
              fprint("  > %s ~ %s", subc, cmdTable[k].desc[subc])
            else
              fprint("  > %s", subc)
            end
          end
        end
        result = ""
      else
        -- [CMD]
        result = format("[CMD] > %s", k)
      end
      if result ~= "" then
        if cmdTable[k].desc then
          result = format("%s ~ %s", result, cmdTable[k].desc)
        end
        if cmdTable[k].set then
          print(result)
          for child in pairs(cmdTable[k].set) do
            -- [PROPERTY]
            if cmdTable[k].child[child].desc then
              fprint("  [PROPERTY] > %s <set value/reset> ~ %s\n    [CURRENT VALUE] > %s", child, cmdTable[k].child[child].desc, tostring(cmdTable[k].set[child].value))
            else
              fprint("  [PROPERTY] > %s <set value/reset>\n    [CURRENT VALUE] > %s", child, tostring(cmdTable[k].set[child].value))
            end
          end
        else
          print(result)
        end
      end
    end
    print("") -- new line
    return true
  end

  local function clearFunc()
    local strings = ui.output.getLines()
    strings.clear()
    return true
  end

  local function terminateFunc()
    os.execute([[TASKKILL /F /IM EliteDangerous32.exe /T]])
    closeCE()
    return true
  end

  local function updateFunc()
    local file = datadir .. "data.lua"
    local function customUpdate()
      local dataUrl = [[http://urealm.eu/data]]
      httpGet(dataUrl, "data.lua")
      return true
    end
    if (isFile(file)) then
      print("")
      print("Updater: Starting updating process...")
      local oldData = readFile(file)
      if customUpdate() then
        if oldData ~= readFile(file) then
          print("Updater: Data updated! In order for it to take effect, you have to restart the script and game.")
        else
          print("Updater: Script is already up to date!")
        end
      else
        print("Updater: Something went wrong :/")
      end
      return true
    else
      if customUpdate() then
        print("Updater: Data updated! In order for it to take effect, you have to restart the script and game.")
      else
        print("Updater: Something went wrong :/")
      end
      return true
    end
  end

  local function autoupdateFunc()
    if script.settings.Value['autoupdate'] ~= "on" then
      script.settings.Value['autoupdate'] = "on"
      print("Updater: Auto updating enabled!")
      return updateFunc()
    else
      script.settings.Value['autoupdate'] = "off"
      print("Updater: Auto updating disabled!")
      return true
    end
  end

  local function breakLogoffsky()
    local pwszObjectName = readString(readInteger(ESP + 12), 256, true)
    if ((pwszObjectName ~= nil) and (string.find(pwszObjectName, "elite/commander/death") ~= nil)) then
      EIP = getAddress("ntdll.RtlExitUserProcess")
      print("\nCMDR death detected! Terminating ED before flag gets sent. Time of death: " .. os.date('%d/%m/%y - %H:%M'))
    end
  end

  local function breakDamage()
    local pthis = ECX
    local caller = readPointer(ESP)
    local incomingPointer = EDI
    local damageDoneTo = ""
    local multiplier = cmdTable['damage'].multiplier
    local multiply = cmdTable['damage'].multiply
    local isPlayerDamage = 1
    writeFloat(multiplier, 1.0)

    if cmdTable['onlyyou'].state then
      isPlayerDamage = readBytes(cmdTable['onlyyou'].flag, 1, false)
    end

    if (caller == script.shieldCaller) then
      local shieldModulePtr = pthis - 0xB8
      local hullRefPtr = shieldModulePtr + 91 * 4
      local hullPtr = readAddress(hullRefPtr)
      local npcFlag = isNPC(hullPtr)

      if not npcFlag then
        if cmdTable['noshielddmg'].state then
          writeFloat(multiplier, 0.0)
        elseif cmdTable['someshielddmg'].state then
          local curShieldValPtr = shieldModulePtr + 920
          local maxShieldValPtr = shieldModulePtr + 888
          local xoredCurShieldVal = bXor(readAddress(curShieldValPtr), readAddress(curShieldValPtr + 4)) -- divided it into two lines for readablility
          local xoredMaxShieldVal = bXor(readAddress(maxShieldValPtr), readAddress(maxShieldValPtr + 4)) -- divided it into two lines for readablility
          local curShieldVal = hex2float(xoredCurShieldVal)
          local maxShieldVal = hex2float(xoredMaxShieldVal)

          if ((curShieldVal > 50.0) and (curShieldVal > maxShieldVal * 0.5)) then
            writeFloat(multiplier, getRandomFloat(0.2, 0.25))
          elseif ((curShieldVal > 50.0) and (curShieldVal > maxShieldVal * 0.15)) then
            writeFloat(multiplier, getRandomFloat(0.1, 0.15))
          else
            writeFloat(multiplier, 0.0)
          end
        end
        damageDoneTo = "Player | Shield"
      elseif (npcFlag and cmdTable['multiply'].state and (isPlayerDamage == 1)) then
        writeFloat(multiplier, getRandomFloat(cmdTable['multiply'].set['minmul'].value, cmdTable['multiply'].set['maxmul'].value))
        damageDoneTo = "NPC | Shield"
      end
    elseif (caller == script.hullCaller) then
      local hullPtr = pthis - 152 + 27 * 4
      local npcFlag = isNPC(hullPtr)

      if (not npcFlag and cmdTable['nohulldmg'].state) then
        writeFloat(multiplier, 0.0)
        damageDoneTo = "Player | Hull"
      elseif (npcFlag and cmdTable['multiply'].state and (isPlayerDamage == 1)) then
        writeFloat(multiplier, getRandomFloat(cmdTable['multiply'].set['minmul'].value, cmdTable['multiply'].set['maxmul'].value))
        damageDoneTo = "NPC | Hull"
      end
    elseif (caller == script.moduleCaller) then
      local shipObj = readAddress(pthis + 276 - 136)
      local npcFlag = readBytes(shipObj + 152, 1, false)

      if (not npcFlag and cmdTable['nomoddmg'].state) then
        writeFloat(multiplier, 0.0)
        damageDoneTo = "Player | Module"
      elseif (npcFlag and cmdTable['multiply'].state and (isPlayerDamage == 1)) then
        writeFloat(multiplier, getRandomFloat(cmdTable['multiply'].set['minmul'].value, cmdTable['multiply'].set['maxmul'].value))
        damageDoneTo = "NPC | Module"
      end
    else
      log(format("Other Caller! - IncomingPointer: 0x%X - Caller: 0x%X", incomingPointer, caller))
    end

    if (readFloat(multiplier) == 1.0) then
      --log("Normal Damage!")
    elseif (readFloat(multiplier) > 1.0) then
      log(format("%s Damage got multiplied by %.1f - Incoming Pointer: 0x%X - Caller: 0x%X", damageDoneTo, readFloat(multiplier), incomingPointer, caller))
    elseif (readFloat(multiplier) < 1.0 and readFloat(multiplier) ~= 0.0) then
      log(format("%s Damage got downscaled by %.1f - Incoming Pointer: 0x%X - Caller: 0x%X", damageDoneTo, 1 / readFloat(multiplier), incomingPointer, caller))
    else
      log(format("%s Damage got nullified! - Incoming Pointer: 0x%X - Caller: 0x%X", damageDoneTo, incomingPointer, caller))
    end
    EIP = multiply
  end

  local function customDamage()
    autoAssemble([[
    alloc(_multiplier,8)
    alloc(_multiply,64)
    registersymbol(_multiplier)
    registersymbol(_multiply)
    _multiply:
    FMUL dword ptr [_multiplier]
    RET 4
    ]])
    cmdTable['damage'].multiplier = getAddress('_multiplier')
    writeFloat(cmdTable['damage'].multiplier, 1.0)
    cmdTable['damage'].multiply = getAddress('_multiply')
  end

  local function minmulFunc(val)
    if type(tonumber(val)) == 'number' then
      cmdTable['multiply'].set['minmul'].value = val
      return "minmul: set to " .. val
    else
      return "minmul: error, please enter a number"
    end
  end

  local function maxmulFunc(val)
    if type(tonumber(val)) == 'number' then
      cmdTable['multiply'].set['maxmul'].value = val
      return "maxmul: set to " .. val
    else
      return "maxmul: error, please enter a number"
    end
  end

  local function breakPower()
    writeFloat(EBX + 0x3C, 10.0)
  end

  local function breakEnergy()
    if cmdTable['energyreduse'].state then
      writeFloat(ESP + 0x8, readFloat(ESP + 0x8) * getRandomFloat(0.1, 0.15))
    elseif cmdTable['energynouse'].state then
      writeFloat(ESP + 0x8, 0.0)
    end
  end

  local function breakHdcharge()
    local hdTimerAddr = EDI + 0x1E8
    writeFloat(hdTimerAddr, 0.0)
    log("nulled hd timer")
  end

  local function breakSccharge()
    local scTimerAddr = EAX + 0x14
    local scTimerValue = readFloat(scTimerAddr)
    if scTimerValue then
      writeFloat(scTimerAddr, 0.0)
      log("nulled sc timer")
    end
  end

  local function breakNomasslock()
    local massMultiplierAddr = EDI + 0x1DC
    local massMultiplierVal = readFloat(massMultiplierAddr)
    if (massMultiplierVal == 0.0) then
      writeFloat(massMultiplierAddr, 1.0)
    end
    log("removed masslock")
  end

  local function breakInfheatsink()
    local timerElapsedAddr = EBX + 0xC0
    writeFloat(timerElapsedAddr, 0.0)
    log("nulled hs timer")
  end

  local function breakFuel()
    if cmdTable['fuelreduse'].state then
      local fuelUsageAddr = ESP + 0x14
      local fuelUsageVal = readFloat(fuelUsageAddr)
      writeFloat(fuelUsageAddr, fuelUsageVal * getRandomFloat(0.2, 0.5))
    elseif cmdTable['fuelnouse'].state then
      local fuelUsageAddr = ESP + 0x14
      writeFloat(fuelUsageAddr, 0.0)
    end
  end

  local function breakJumpFuel()
    local toTargetFuelUsageAddr = ESP + 0x40 --0x64
    local toTargetFuelUsageVal = readFloat(toTargetFuelUsageAddr)
    if cmdTable['jumpnouse'].state then
      writeFloat(toTargetFuelUsageAddr, 0.0)
      log("nullified fuel usage (jump)")
    elseif cmdTable['jumpreduse'].state then
      local multiplier = 1 / cmdTable['jumpreduse'].set['factor'].value
      writeFloat(toTargetFuelUsageAddr, toTargetFuelUsageVal * multiplier)
      log(format("reduced fuel usage by a factor of %.2f!", 1 / multiplier))
    end
  end

  local function factorFunc(val)
      if type(tonumber(val)) == 'number' then
        cmdTable['jumpreduse'].set['factor'].value = val
        return "factor: set to " .. val
      else
        return "factor: error, please enter a number"
      end
  end


  local function breakUdpwatcher()
    local size = readInteger(ESP + 8)
    local buffer = readBytes(readAddress(ESP + 4), size, true)
    if not streamTimer then
      streamSizePerSecond = 0
      streamCountPerSecond = 0
      streamTimer = createTimer()
      timer_setInterval(streamTimer, 1000)
      timer_onTimer(streamTimer, function()
        if (streamSizePerSecond ~= 0) then
          log('UDP Stream | ' .. streamSizePerSecond .. ' Byte(s)/s - ' .. streamCountPerSecond .. ' Packet(s)/s')
          streamSizePerSecond = 0
          streamCountPerSecond = 0
        end
      end)
    end
    if streamSize and streamCount then
      streamSize = streamSize + size
      streamSizePerSecond = streamSizePerSecond + size
      streamCount = streamCount + 1
      streamCountPerSecond = streamCountPerSecond + 1
    else
      streamSize = size
      streamCount = 1
      printStreamStats = function() log(format("Total Size: %i Byte(s) | Packet Count: %i Packet(s)", streamSize, streamCount)) end
    end
    if (streamSizePerSecond > 20000) then
      print('Warning! Your traffic is unusually high! (' .. streamSizePerSecond .. ' Byte(s)/s - ' .. streamCountPerSecond .. ' Packet(s)/s)')
      beep()
    end
    local fudp = assert(io.open(udpfilename, "a+"))
    fudp:write('Size: ' .. size .. ' Bytes | Buffer: ' .. format('%x', byteTableToDword(buffer)) .. '\n')
    fudp:close()
  end

  onlyyouAA = [[
  onlyyou:
  PUSH EAX
  MOV EAX,[ECX+01C1]
  MOV [_dmgSrcFlag],EAX
  POP EAX
  JMP _onlyyou
  ]]

  local function onlyyouFunc()
    autoAssemble([[
    alloc(_dmgSrcFlag,4)
    registersymbol(_dmgSrcFlag)
    ]])
    cmdTable['onlyyou'].flag = getAddress("_dmgSrcFlag")
  end

  addCMD("help", helpFunc, "Prints this message...") --
  addCMD("clear", clearFunc, "Clears the console output") --
  addCMD("exit", terminateFunc, "Terminates CE and ED") --
  addCMD("update", updateFunc, "Updates the script data (restart required!)") --
  addCMD("autoupdate", autoupdateFunc, "Toggles auto updating!") --
  addBreakpoint("logoffsky", getAddress("WINHTTP.WinHttpOpenRequest"), breakLogoffsky, "Terminates ED when detecteting CMDR death!") --
  addPatch("interdictionpatch", dec(24521157), "90 E9", "Disables interdiction") --
  addBreakpoint("damage", dec(27145118+18), breakDamage, nil, nil, nil, nil, customDamage) --
  addToggle("multiply", "Multiplies damage done to ships", "damage") --
  addHook("onlyyou", dec(26057904), onlyyouAA, onlyyouFunc, "Only the damage you are dealing will be multiplied!", "multiply") --
  addProperty("minmul", 3.0, "multiply", minmulFunc, "Min multiplier value") --
  addProperty("maxmul", 4.0, "multiply", maxmulFunc, "Max multiplier value") --
  addToggle("nohulldmg", "Damage dealt to YOUR hull will be negated", "damage") --
  addToggle("someshielddmg", "Damage dealt to YOUR shields will be downscaled", "damage") --
  addToggle("noshielddmg", "Damage dealt to YOUR shields will be negated", "damage") --
  addToggle("nomoddmg", "Damage dealt to YOUR modules will be negated", "damage") --
  addBreakpoint("energy", dec(27098368), breakEnergy) --
  addToggle("energyreduse", "Reduces the energy used by weapons, shields and engine", "energy") --
  addToggle("energynouse", "Negates the energy used by weapons, shields and engine", "energy") --
  addBreakpoint("hdcharge", dec(23457010), breakHdcharge, "Disables hyper drive charging time") --
  addBreakpoint("sccharge", dec(23457187), breakSccharge, "Disables super cruise charging time") --
  addPatch("nocooldown", dec(23453071), "90 90", "Disables cooldown time after jumps") --
  addBreakpoint("nomasslock", dec(23452539), breakNomasslock, "Disables masslock") --
  addPatch("noscoop", dec(24514049), "90 90", "Removes traditional scooping. Just open your bay and target the container!") --
  addBreakpoint("infheatsink", dec(24672065), breakInfheatsink, "Heatsinks will last until you enter a new instance or dock") --
  addBreakpoint("fuel", dec(27097952), breakFuel) --
  addToggle("fuelreduse", "Reduces fuel usage in normal flight", "fuel") --
  addToggle("fuelnouse", "Negates fuel usage in normal flight", "fuel") --
  addBreakpoint("jumpfuel", dec(23455933), breakJumpFuel) --
  addToggle("jumpreduse", "Reduces fuel usage by a variable factor for jumps", "jumpfuel") --
  addProperty("factor", 10.0, "jumpreduse", factorFunc, "Factor for jump fuel reduction") --
  addToggle("jumpnouse", "Negates fuel usage for jumps", "jumpfuel") --
  addPatch("freezeammo", dec(24693548), "90 90", "Freezes some ammo types") --
  addBreakpoint("udpwatcher", dec(18428352), breakUdpwatcher, "Counts your UDP traffic and warns you if it exceeds the threshold") --
  script.shieldCaller = dec(27209904)
  script.hullCaller = dec(24210353)
  script.moduleCaller = dec(28286535)
  moduleFunc()
  aliasFunc()
  settingsFunc()
  if script.settings.Value['autoupdate'] == "on" then updateFunc() end
end,

breakpoint = function()
  debugProcess(2)

  for k,v in ordered(breakpointTable) do
    if EIP == cmdTable[k].address then
      cmdTable[k].breakpointFunc()
      debug_continueFromBreakpoint(co_run)
      return 1
    end
  end

  fprint("error, unexpected break @ 0x%X! Continuing execution...", EIP)
  debug_continueFromBreakpoint(co_run)
  return 1
end,

attach = function()
  local once = true
  local detachWD = {}

  function detachWD.hook()
    detachWD.waitForDebugEvent = getAddress("KERNEL32.WaitForDebugEvent")
    if (not detachWD.waitForDebugEvent) or (detachWD.waitForDebugEvent == 0) then
      print("Failed to hook WaitForDebugEvent!")
      return false
    end
    debugger_onBreakpoint = detachWD.OnBreakpoint
    debugProcess()
    debug_setBreakpoint(detachWD.waitForDebugEvent)
    fprint("Successfully hooked WaitForDebugEvent @ 0x%X!", detachWD.waitForDebugEvent)
    return true
  end

  function detachWD.OnBreakpoint()
    if (EIP == detachWD.waitForDebugEvent) then
      autoAssemble([[
      alloc(_edPID, 4)
      registersymbol(_edPID)
      ]])
      writeInteger(getAddress("_edPID"), script.edPID)

      autoAssemble([[
      alloc(detour,100)

      detour:
      PUSH [_edPID]
      CALL KERNEL32.DebugActiveProcessStop
      JMP ntdll.RtlExitUserProcess

      KERNEL32.WaitForDebugEvent:
      JMP detour
      ]])

      script.detached = true
      print("Detached and Terminated WatchDog! WDPID: " .. script.wdPID .. " | EDPID: " .. script.edPID)
      debug_removeBreakpoint(detachWD.waitForDebugEvent)

      debug_continueFromBreakpoint(co_run)
      return 1
    end
  end

  function start()
    if once then
      print("Preparing for script...")
      once = false
      attachWD()
    end
  end

  function waitForWD(timer)
    if (getProcessIDFromProcessName(script.wd)) and (getProcessIDFromProcessName(script.ed)) then
      object_destroy(timer)
      script.edPID = getProcessIDFromProcessName(script.ed)
      script.wdPID = getProcessIDFromProcessName(script.wd)
      print("Found the process WatchDog.exe!")
      print("Press HOME/POS1 upon seeing the main menu to continue...")
      local forceStart = createNonRepHotkey(function() start() end, VK_HOME)
    end
  end

  function attachWD()
    openProcess(script.wdPID)
    print("Successfully attached to WatchDog! PID = " .. getOpenedProcessID())
    if detachWD.hook() then
      local t2 = createTimer()
      timer_setInterval(t2, 1000)
      timer_onTimer(t2, attachED)
      print("Waiting for WatchDog to be detached and terminated...")
    else
      print("An error occurred! Please contact the script creator!")
    end
  end

  function attachED(timer)
    if script.detached then
      object_destroy(timer)
      openProcess(script.edPID)
      print("Successfully attached to Elite: Dangerous! PID = " .. getOpenedProcessID())
      script.header = dissectPEHeader(script.ed)
      script.base = script.header.base
      script.extbase = 16908288

      local function AOBScan(pattern)
        local module = script.ed
        local startaddr = getAddress(module)
        local endaddr = startaddr + getModuleSize(module)
        local memscan = createMemScan()
        memscan_returnOnlyOneResult(memscan, true)
        memscan_firstScan(memscan, soExactValue, vtByteArray, 0, pattern, "", startaddr, endaddr, "*X*W*C", 0, "", true, true, false, false)
        memscan_waitTillDone(memscan)
        return memscan_getOnlyResult(memscan)
      end

      if AOBScan("51 53 8B 5C 24 10 55 8B 6C 24 20") == dec(21909824) then
        print("Signature check: passed!")

        -- codecheck2
        local address = dec(17483088)
        local k = "CodeCheck"
        local size = 0
        repeat
          size = size + getInstructionSize(address + size)
        until size >= 5
        local returnaddress = address + size

        local ReadBytes = readBytes(address, size, true)
        local bytes = ""
        for _, data in pairs(ReadBytes) do
          local hex = format("%x", data)
          if (hex:len() == 1) then
            hex = format("0%s", hex)
          end
          bytes = format("%s %s", bytes, hex)
        end

        local nops = ""
        if (size > 5) then
          for i = 1, size - 5 do
            nops = format("%s %s", nops, "90")
          end
          nops = format("%s%s", "db", nops)
        end

        local function patch(address, bytes)
          local aaenable = format("%x:\ndb %s", address, bytes)
          autoAssemble(aaenable)
        end

        local start = script.base + script.header['.text'].pointerToRawData
        local length = script.header['.text'].sizeOfRawData + 0x1000
        local aacopytext = format([===[
        alloc(_textcopy,%i)
        alloc(_rstart,4)
        alloc(_rend,4)
        registersymbol(_textcopy)
        registersymbol(_rstart)
        registersymbol(_rend)
        _textcopy:
        readmem(%x,%i)
        _rstart:
        dd %x
        _rend:
        dd %x]===],
        length, start, length, start, start + length)
        autoAssemble(aacopytext)

        local aahook = format([===[
        alloc(_%s,%i)
        alloc(%s,1024)
        alloc(__%s,4)
        alloc(_copyAddress,4)
        registersymbol(%s)
        registersymbol(_%s)
        registersymbol(__%s)
        registersymbol(_copyAddress)
        _%s:
        db%s
        JMP [__%s]
        %x:
        JMP %s
        %s
        __%s:
        dd %x
        _copyAddress:
        dd %x]===],
        k, size + 6, k, k, k, k, k, k, bytes, k, address, k, nops, k, returnaddress, getAddress("_textcopy"))
        autoAssemble(aahook)

        autoAssemble([[
        CodeCheck:
        MOV EAX,[ESP+04]
        CMP EAX,[_rstart]
        JB _CodeCheck
        CMP EAX,[_rend]
        JGE _CodeCheck
        ADD EAX,[_copyAddress]
        SUB EAX,[_rstart]
        MOV [ESP+04],EAX
        JMP _CodeCheck
        ]])

        patch(dec(24829499), '90 90 90 90 90 90 90 90 90 90 90 90 90 90 90 90') -- codecheck
        patch(dec(24829698+4), '01 00 01') -- flag
        patch(dec(27210057), 'F3 0F 10 C1 90 90') -- shieldC
        patch(dec(28268506), 'EB') -- energyC

        print("Almost ready ...\n")
        script.init()
      else
        print("error, not matching signatures! terminating script")
      end
    end
  end

  t1 = createTimer()
  timer_setInterval(t1, 1000)
  timer_onTimer(t1, waitForWD)
  print("Waiting for WatchDog to start...")
end,

init = function()
  -- -------------------------------------------------------------------------- --
  -- Adding hacks
  -- -------------------------------------------------------------------------- --
  script.hacks = loadData("hacks")
  script.hacks()

  -- -------------------------------------------------------------------------- --
  -- Loading breakpoint function
  -- -------------------------------------------------------------------------- --
  script.onBreakpoint = loadData("breakpoint")
  debugger_onBreakpoint = script.onBreakpoint

  local function navi(thread)
    thread.name = 'Navi Thread'
    local navifile = eliteoutdir .. "navi.txt"

    while true do
      if (naviToggle) then
        local f = assert(io.open(navifile, "w+"))
        f:write(format("%f\n%f\n%f", readFloat(cmdTable['compass'].compassX), readFloat(cmdTable['compass'].compassY), readFloat(cmdTable['compass'].compassZ)))
        f:close()
      else
        sleep(2000)
      end
    end
  end
  --createNativeThread(navi)
end
}
